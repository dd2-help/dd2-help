(ns dd2-drops.dev
  (:require [dd2-drops.core :as dd2]
            [figwheel.client :as fw]))

(fw/start {:on-jsload dd2/run
           :websocket-url "ws://localhost:3449/figwheel-ws"})
