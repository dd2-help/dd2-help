The item called `<ITEM NAME HERE>` has some problems:

*   [ ] The name should be corrected to `<CORRECT NAME HERE>`.
*   [ ] It should be tagged as `<WHICH HERO?>` and `<TOWER OR DPS?>`.
*   [ ] It really drops from `<CORRECT MAP HERE>`.
*   [ ] Its passives should be corrected to `<CORRECT PASSIVES HERE>`.

I've attached a screenshot of the item dropped here:

`<ATTACH SCRENSHOT OF ITEM HERE>`
