(ns dd2-drops.sorting)

(defn sorted-set-by-field
  [field elements]
  (apply sorted-set-by
     #(compare (field %1) (field %2))
     elements))
