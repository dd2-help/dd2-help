(ns dd2-drops.core
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require [clojure.set :refer [intersection]]
            [clojure.spec :as s]
            [clojure.string :refer [capitalize includes? lower-case upper-case]]
            [dd2-drops.maps :refer [maps]]
            [dd2-drops.searching :refer [relevant-map?]]
            [dd2-drops.sorting :refer [sorted-set-by-field]]
            [reagent.core :as reagent]
            [re-frame.core :refer [reg-event-db
                                   path
                                   reg-sub
                                   dispatch
                                   dispatch-sync
                                   subscribe]]))

(def initial-state
  {:maps maps
   :filters
   {:class-type #{:abyss-lord :apprentice :ev2 :gun-witch :huntress :lavamancer :monk :mystic :squire}
    :search ""
    :map-type #{:common :defense :incursion}}})


;; -- Event Handlers ----------------------------------------------------------


(reg-event-db
  :clear
  (fn
    [db _]
    (merge db initial-state)))

(reg-event-db                 ;; setup initial state
  :initialize                     ;; usage:  (dispatch [:initialize])
  (fn
    [db _]
    (merge db initial-state)))    ;; what it returns becomes the new state

(reg-event-db
  :search
  (fn
    [db [_ value]]
    (assoc-in db [:filters :search] value)))

(reg-event-db
  :map-type
  (fn
    [db [_ value]]
    (let [present? ((get-in db [:filters :map-type]) value)]
      (update-in db [:filters :map-type] (if present? disj conj) value))))

(reg-event-db
  :class-type
  (fn
    [db [_ value]]
    (let [present? ((get-in db [:filters :class-type]) value)]
      (update-in db [:filters :class-type] (if present? disj conj) value))))

;; -- Subscription Handlers ---------------------------------------------------


(reg-sub
  :search
  (fn
    [db _]
    (-> db :filters :search)))

(reg-sub
  :map-type
  (fn
    [db _]
    (-> db :filters :map-type)))

(reg-sub
  :class-type
  (fn
    [db _]
    (-> db :filters :class-type)))

(reg-sub
  :filters
  (fn
    [db _]
    (:filters db)))

;; -- View Components ---------------------------------------------------------

(defn stats
  [description parameters]
  (into [:span]
   (reduce
     (fn [s next]
       (if (string? next)
         (conj s next)
         (let [[low high] next]
           (conj s [:span "[" [:b.low low] " - " [:b.high high] "]"]))))
     []
     description)))

(defn passive
  [{:keys [description name parameters]}]
  [:li.passive
   name " - " [stats description parameters]])

(defn badges
  [{:keys [type]}]
  (let [full (-> type name capitalize (str "-type map"))
        short (->> full upper-case (take 3) (apply str))]
    [:div.badges [:abbr {:title full} short]]))

(defn title
  []
  [:h1 "DD2 Drops"])

(defn class-filter
  [class]
  (let [class-type (subscribe [:class-type])]
    [:button.filter.icon
     {:type "button"
      :class (when-not (@class-type class) "inactive")
      :on-click #(dispatch [:class-type class])}
     [:img.filter-img
      {:src (str "img/" (name class) ".png")}]]))

(defn map-filter
  [type]
  (let [map-type (subscribe [:map-type])]
    [:button.filter
     {:type "button"
      :class (when-not (@map-type type) "inactive")
      :on-click #(dispatch [:map-type type])}
     (capitalize (name type))]))

(defn filters
  []
  (let [map-type (subscribe [:map-type])]
    [:div
     [:div.filters
      [class-filter :abyss-lord]
      [class-filter :apprentice]
      [class-filter :ev2]
      [class-filter :gun-witch]
      [class-filter :huntress]
      [class-filter :lavamancer]
      [class-filter :monk]
      [class-filter :mystic]
      [class-filter :squire]]
     [:div.filters
      [map-filter :common]
      [map-filter :defense]
      [map-filter :incursion]]]))

(defn search
  []
  (let [current (subscribe [:search])]
    (fn []
      [:div.search
       [:input
        {:value @current
         :type "text"
         :placeholder "Search for map or item"
         :on-change #(dispatch [:search (-> % .-target .-value)])}]
       [:button.reset
        {:type "reset"
         :on-click #(dispatch [:clear])}
        "CLEAR"]])))

(defn render-drops
  [{:keys [name passives]}]
  [:li.drop
   [:h3 name]
   (into [:ul.drops] (map passive (sorted-set-by-field :name passives)))])

(defn render-map
  [{:keys [drops hidden image name type] :as m}]
  [:article
   {:class (when hidden "hidden")}
   [:div.item-header
    [:img {:src (str "img/" image)}]
    [:div
     [:h2 name]
     [:div [badges m]]]]
   (into [:ul.drops] (map render-drops (sorted-set-by-field :name drops)))])

(defn shown-maps
  []
  (let [filters (subscribe [:filters])]
    (into [:div]
      (->> (sorted-set-by-field :name maps)
        (map #(if (relevant-map? % @filters)
                %
                (assoc % :hidden true)))
        (map render-map)))))

(defn page
  []
  [:section
   [title]
   [search]
   [filters]
   [shown-maps]])


;; -- Entry Point -------------------------------------------------------------


(defn ^:export run
  []
  (dispatch-sync [:initialize])
  (reagent/render [page]
    (js/document.getElementById "app")))
