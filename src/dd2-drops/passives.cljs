(ns dd2-drops.passives)

;;; Abyss Lord

(def abyss-knight-damage
  {:class #{:abyss-lord}
   :description
   ["Abyss Knight Ability damage increased by "
    [0 24]
    "%"]
   :name "Abyss Knight Damage"})

(def abyss-knights-mana-cost
  {:class #{:abyss-lord}
   :description
   ["Abyss Knight's mana cost is reduced by "
    [0 7]
    "%."]
   :name "Abyss Knight's Mana Cost"})

(def abyss-stone-mana-cost
  {:class #{:abyss-lord}
   :description
   ["Abyss Stone's mana cost is reduced by "
    [0 7]
    "%."]
   :name "Abyss Stone Mana Cost"})

(def archer-damage
  {:class #{:abyss-lord}
   :description
   ["Skeletal Archer's damage is increased by "
    [0 10]
    "%."]
   :name "Archer Damage"})

(def breath-damage
  {:class #{:abyss-lord}
   :description
   ["Damage of the Ramster's Direct Command increased by "
    [0 12]
    "%."]
   :name "Breath Damage"})

(def colossal-health
  {:class #{:abyss-lord}
   :description
   ["Colossus health is increased by "
    [0 6]
    "%."]
   :name "Colossal Health"})

(def demon-scythe
  {:class #{:abyss-lord}
   :description
   ["Secondary Attack summons the Demon Scythe that pierces 5 targest dealing 90% Ability Power as magical damage. Charged attacks summon a bigger scythe that pierces 10 targets and deals 300% Ability Power as magical damage"]
   :name "Demon Scythe"})

(def devour
  {:class #{:abyss-lord}
   :description
   ["Adds "
    [0 12]
    "% of Hero Damage to Primary Attack as Magical Fire Damage."]
   :name "Devour"})

(def direct-command-mana-cost
  {:class #{:abyss-lord}
   :description
   ["Direct Command's mana cost is reduced by "
    [0 7]
    "%."]
   :name "Direct Command Mana Cost"})

(def electric-fingers
  {:class #{:abyss-lord}
   :description
   ["Skeletal Orcs have a 30% chance to electrify their target dealing "
    [0 400]
    "% of your Defence Health as Lightning Damage. This effect bounces to 3 targets."]
   :name "Electric Fingers"})

(def emburst-assault
  {:class #{:abyss-lord}
   :description
   ["Secondary Attack summons meteors that deal "
    [0 350]
    "% of Ability Power as damage. Charge to summon 5 lava fissures for "
    [0 850]
    "% of Ability Power as Magical Fire Damage burning enemies for "
    [0 60]
    "% of Hero Damage for 6s."]
   :name "Emburst Assault"})

(def explosive-arrows
  {:class #{:abyss-lord}
   :description
   ["Archers have a 25% chance to fire an explosive arrow that detonates on impact dealing "
    [0 200]
    "% of your Defence Power as extra damage."]
   :name "Explosive Arrows"})

(def ramster-breath-range
  {:class #{:abyss-lord}
   :description
   ["Range of the Ramster's Direct Command increased by "
    [0 12]
    "%."]
   :name "Ramster Breath Range"})

(def shattering-stones
  {:class #{:abyss-lord}
   :description
   ["After an Abyss Stone expires, it detonates dealing "
    [0 450]
    "% of your Ability Power as damage to nearby enemies."]
   :name "Shattering Stones"})

(def skeletal-orc-health
  {:class #{:abyss-lord}
   :description
   ["Skeletal Orc's health is increased by "
    [0 6]
    "%."]
   :name "Skeletal Orc Health"})

;;; Apprentice

(def ancient-magics
  {:class #{:apprentice}
   :description
   ["Mana Bomb damage increased by "
    [0 19]
    "%."]
   :name "Ancient Magics"})

(def arcane-resilience
  {:class #{:apprentice}
   :description
   ["Arcane Barrier Reflects "
    [0 1500]
    "% Defense Health as damage over 8s, and has a "
    [0 75]
    "% chance to fire meteors dealing "
    [0 1300]
    "% Defense Health as damage."]
   :name "Arcane Resilience"})

(def arcanist
  {:class #{:apprentice}
   :description
   ["Increases maximum marked targets by "
    [0 1]
    "."]
   :name "Arcanist"})

(def dark-mage-slayer
  {:class #{:apprentice}
   :description
   ["Defeating a Dark Mage grants a charge. Fully-charged Secondary Attacks consumes charges and summon skeletons to stun enemies for "
    [0 5]
    "s."]
   :name "Dark Mage Slayer"})

(def fault-line
  {:class #{:apprentice}
   :description
   ["Earthshatter explosion radius increased by "
    [0 20]
    "%."]
   :name "Fault Line"})

(def frosty-power
  {:class #{:apprentice}
   :description
   ["Frostbite Towers Chill increases Tower Defense Power by +"
    [0 50]
    "% of total Defense Power."]
   :name "Frosty Power"})

(def ghostly-aura
  {:class #{:apprentice}
   :description
   ["Reduces overall damage taken by "
    [0 90]
    "%, increases movement speed by "
    [0 160]
    "%, and reduces enemy movement speed."]
   :name "Ghostly Aura"})

(def lunar-portal
  {:class #{:apprentice}
   :description
   ["Secondary Attacks summon up to 4 Lunar Portals from a far away land that deal 225% Ability Power as magical damage per second for 12s."]
   :name "Lunar Portal"})

(def mana-portal
  {:class #{:apprentice}
   :description
   ["Mana Bomb creates a shield absorbing "
    [0 500]
    "% Ability Power damage, reflects "
    [0 800]
    "% Ability Power unresisted damage over 3s, fires meteors dealing "
    [0 500]
    "% Ability Power unresisted damage."]
   :name "Mana Portal"})

(def mighty-wind
  {:class #{:apprentice}
   :description
   ["Tornado knockup duration increased by "
    [0 19]
    "%."]
   :name "Mighty Wind"})

(def pyromania
  {:class #{:apprentice}
   :description
   ["Flameburst Tower and Flamethrower Tower damage increased by "
    [0 3]
    "%."]
   :name "Pyromania"})

(def ravenhost
  {:class #{:apprentice}
   :description
   ["Basic attacks add a Raven Stack. At 10 stacks, spend all stacks to fire a magical Raven dealing "
    [0 60]
    "% Hero Damage that bounces between 5 targets."]
   :name "Ravenhost"})

(def snowstorm
  {:class #{:apprentice}
   :description
   ["Frostbite Tower's chill slow is increased by "
    [0 19]
    "%."]
   :name "Snowstorm"})

(def specter-scepters
  {:class #{:apprentice}
   :description
   ["Secondary fire summons up to "
    [0 6]
    " Halberds that do "
    [0 600]
    "% of Ability Power as damage."]
   :name "Specter Scepters"})

(def starlight-burst
  {:class #{:apprentice}
   :description
   ["Arcane Volley damage increased by "
    [0 3]
    "%."]
   :name "Starlight Burst"})

(def weakness-curse
  {:class #{:apprentice}
   :description
   ["Curses an area on hit. Enemies within the curse have their damage reduced to "
    [0 35]
    "%."]
   :name "Weakness Curse"})

(def weaver
  {:class #{:apprentice}
   :description
   ["Apprentice Defenses that cause damage now mark their targets."]
   :name "Weaver"})

;; EV2

(def atomic-madness
  {:class #{:ev2}
   :description
   ["Atomic Launcher has an "
    [0 50]
    "% chance to fire 2 extra shots in a small spread. These deal 300% of Hero Damage stat as damage."]
   :name "Atomic Madness"})

(def big-boy
  {:class #{:ev2}
   :description
   ["The first mine you place will always be a Big Boy. Big Boy mines do an extra "
    [0 1500]
    "% of your Hero Damage stat as added damage in double the area."]
   :name "Big Boy"})

(def celebration
  {:class #{:ev2}
   :description
   ["Fires two homing projectiles that detonate in a festive explosion dealing 72% Hero Damage as magical damage over a small area."]
   :name "Celebration!!!"})

(def deathly-heat
  {:class #{:ev2}
   :description
   ["Death from Above's heat generation is reduced by "
    [0 6]
    "%."]
   :name "Deathly Heat"})

(def dummy-damage
  {:class #{:ev2}
   :description
   ["EV2's decoy deals an extra "
    [0 60]
    "% of your Ability Power stat as more damge to enemies."]
   :name "Dummy Damage"})

(def frosty-proton-node
  {:class #{:ev2}
   :description
   [[0 3]
    "% chance for the Proton Node to freeze all enemies it touches for 3 seconds."]
   :name "Frosty Proton Node"})

(def grav-bot-capacity-increase
  {:class #{:ev2}
   :description
   ["EV2 upgrades her Gravity Bot launcher to store an additional "
    [0 4]
    " Gravity Bots."]
   :name "Grab Bot Capacity Increase"})

(def grav-bot-compensator
  {:class #{:ev2}
   :description
   ["EV2 improves her Grav Bot Launcher reducing the amount of heat generated by "
    [0 6]
    "%."]
   :name "Grav Bot Compensator"})

(def grav-bot-detonation-radius
  {:class #{:ev2}
   :description
   ["Increases the detonation radius of EV2's Gravity Bots by "
    [0 60]
    "%."]
   :name "Grav Bot Detonation Radius"})

(def mdl-discharge
  {:class #{:ev2}
   :description
   ["Picking up Mega Death Laser causes EV2 to gain a steroid that improves her movement speed by 250 and her Hero Damage by "
    [0 50]
    "% for 15s."]
   :name "MDL Discharge"})

(def proton-cannon-heat-generation
  {:class #{:ev2}
   :description
   ["Heat generation rate of Proton Cannon is reduced by "
    [0 6]
    "%."]
   :name "Proton Cannon Heat Generation"})

(def proton-charge-width
  {:class #{:ev2}
   :description
   ["Width of EV2's Proton Charge is increased by "
    [0 13]
    "%."]
   :name "Proton Charge Width"})

(def reflect-beam-nodes
  {:class #{:ev2}
   :description
   ["EV2's Reflect Beam can sustain even more nodes increasing it by: "
    [0 1]
    "."]
   :name "Reflect Beam Nodes"})

(def secondary-heat
  {:class #{:ev2}
   :description
   ["Reduces the cost of her secondary attacks by "
    [0 6]
    "%."]
   :name "Secondary Heat"})

(def torpedo-range
  {:class #{:ev2}
   :description
   ["Range of the Reflect Wall's torpedo explosion is increased by "
    [0 20]
    "%."]
   :name "Torpedo Range"})

(def weapon-manufacturer-charge-rate
  {:class #{:ev2}
   :description
   ["Weapon Manufacturer charges "
    [0 4]
    "% faster."]
   :name "Weapon Manufacturer Charge Rate"})

(def weapon-manufacturer-damage
  {:class #{:ev2}
   :description
   ["Weapon Manufacturer's auras deal "
    [0 5]
    "% more damage."]
   :name "Weapon Manufacturer Damage"})

;;; Gun Witch

(def a360-no-scope
  {:class #{:gun-witch}
   :description
   ["Sniper total critical damage increased by "
    [0 10]
    "%."]
   :name "360 No Scope"})

(def bat-shot
  {:class #{:gun-witch}
   :description
   ["Scatter Sweep damage increased by "
    [0 5]
    "%."]
   :name "Bat Shot"})

(def broom-bash
  {:class #{:gun-witch}
   :description
   ["Blunder Broom Buster duration increased by "
    [0 1]
    "s."]
   :name "Broom Bash"})

(def clean-sweep
  {:class #{:gun-witch}
   :description
   ["Primary Attack random criticals restore "
    [0 5]
    " Broom Power."]
   :name "Clean Sweep"})

(def deadeye
  {:class #{:gun-witch}
   :description
   ["Snipe Crit Chance increased by "
    [0 10]
    "%."]
   :name "Deadeye"})

(def focus-fire
  {:class #{:gun-witch}
   :description
   ["Two at Twice the Price damage increased by "
    [0 5]
    "%."]
   :name "Focus Fire"})

(def frozen-core
  {:class #{:gun-witch}
   :description
   ["Ice Needle damage increased by "
    [0 5]
    "%."]
   :name "Frozen Core"})

(def magazine-reserve
  {:class #{:gun-witch}
   :description
   ["Two at twice the price duration increased by "
    [0 1]
    "s."]
   :name "Magazine Reserve"})

(def meteor-bullets
  {:class #{:gun-witch}
   :description
   ["Rapidly fires Meteor Bullets that pierce one additional target or reflect off one surface for 50% damage."]
   :name "Meteor Bullets"})

(def skeet-shooter
  {:class #{:gun-witch}
   :description
   ["Scatter Sweep range increased by "
    [0 10]
    "%."]
   :name "Skeet Shooter"})

(def sorcerors-apprentice
  {:class #{:gun-witch}
   :description
   ["Broom Nado damage increased by "
    [0 5]
    "%."]
   :name "Sorceror's Apprentice"})

(def witch-mark
  {:class #{:gun-witch}
   :description
   ["Abilities have a "
    [0 20]
    "% chance to place a Witch Mark on each target. Witch Mark increases the damage of the next hit by 100%."]
   :name "Witch Mark"})

(def witch-recover
  {:class #{:gun-witch}
   :description
   [[0 10]
    " Broom Power restored every 5s."]
   :name "Witch Recover"})

(def zerod-in
  {:class #{:gun-witch}
   :description
   ["Snipe random criticals restore "
    [0 20]
    " Broom Power."]
   :name "Zero'd In"})

;;; Huntress

(def breaker
  {:class #{:huntress}
   :description
   ["Reduces target's resistances by "
    [0 20]
    " on hit."]
   :name "Breaker"})

(def call-of-the-phoenix
  {:class #{:huntress}
   :description
   ["Increases Blaze Balloon Defense Crit Chance by "
    [0 10]
    "% and increases Critical Damage by "
    [0 10]
    "% of your Defense Power."]
   :name "Call of the Phoenix"})

(def chlorophyte-arrows
  {:class #{:huntress}
   :description
   ["Fires a column of Chlorophyte Arrows that deal 18% bonus magical earth damage and may reflect off one surface."]
   :name "Chlorophyte Arrows"})

(def envenom
  {:class #{:huntress}
   :description
   ["Poison Dart Tower's poison attack rate increased by "
    [0 19]
    "%."]
   :name "Envenom"})

(def flameageddon
  {:class #{:huntress}
   :description
   ["Piercing Shot damage increased by "
    [0 19]
    "%."]
   :name "Flameageddon"})

(def flashbang
  {:class #{:huntress}
   :description
   ["Concussive Shots stun duration increased by "
    [0 19]
    "%."]
   :name "Flashbang"})

(def from-the-ashes
  {:class #{:huntress}
   :description
   ["3.5 seconds after the Blaze Balloon detonates a phoenix emerges from the flames dealing an extra "
    [300 500]
    "% of your Defense Power as damage."]
   :name "From The Ashes"})

(def gold-shower
  {:class #{:huntress}
   :description
   ["Charged Secondary Attack fires a molten stream "
    [45 90]
    "degrees wide burning for "
    [25 50]
    "% Hero Damage twice per second."]
   :name "Gold Shower"})

(def jester-arrows
  {:class #{:huntress}
   :description
   ["Fires a column of Jester Arrows that pierce up to 5 targets."]
   :name "Jester Arrows"})

(def lady-orc-slayer
  {:class #{:huntress}
   :description
   ["Defeating a Lady Orc grants a charge. Fully-charged Secondary Attacks consumes charges, unleashing a cleaver dealing "
    [0 3]
    "% of your Hero Damage Stat as physical damage."]
   :name "Lady Orc Slayer"})

(def midas-touch
  {:class #{:huntress}
   :description
   ["On hit, enemies move at a "
    [0 60]
    "% reduced rate and burn for "
    [0 120]
    "% Hero Damage a second for 8s."]
   :name "Midas Touch"})

(def mirv
  {:class #{:huntress}
   :description
   ["On hit, flying enemies now have a "
    [50 75]
    "% chance to spawn grenades in a "
    [500 1200]
    " radius that deal "
    [100 180]
    "% of your Hero Damage stat as damage."]
   :name "MIRV"})

(def pumpkinator
  {:class #{:huntress}
   :description
   ["Targets hit with basic attacks are pumpkined. Defeating a pumpkined enemy causes a physical explosion dealing "
    [0 0.6]
    "% Hero Damage. Hitting a pumpkined enemy with a melee attack increases Hero Critical Chance by 5% for 5s"]
   :name "Pumpkinator"})

(def shard-shot
  {:class #{:huntress}
   :description
   ["Concussive Shots fires a piercing shard dealing "
    [0 400]
    "% Ability Power damage, then explodes dealing "
    [0 200]
    "% Ability Power unresisted damage."]
   :name "Harbinger's Fist"})

(def shard-spike
  {:class #{:huntress}
   :description
   [[0 75]
    "% chance for Geyser Trap to fire an exploding shard dealing "
    [0 600]
    "% Defense Power as damage in an area."]
   :name "Shard Spike"})

(def shrapnel
  {:class #{:huntress}
   :description
   ["Concussive Shots and Sticky Nades damage increased by "
    [0 3]
    "%."]
   :name "Shrapnel"})

(def tripwire
  {:class #{:huntress}
   :description
   ["Explosive Trap and Elemental Chaos Trap damage increased by "
    [0 3]
    "%."]
   :name "Tripwire"})

(def vicious-brew
  {:class #{:huntress}
   :description
   ["Oil Flask damage increased by "
    [0 19]
    "%."]
   :name "Vicious Brew"})

(def victory-smell
  {:class #{:huntress}
   :description
   ["Blaze Balloon burn attack rate increased by "
    [0 19]
    "%."]
   :name "Victory Smell"})

(def vipers-bite
  {:class #{:huntress}
   :description
   ["Increases Poison Dart Tower damage by +"
    [0 20]
    "%. On death, poisoned enemies detonate and deal "
    [0 80]
    "% of Defense Power as damage to enemies in the "
    [0 1500]
    " explosion radius."]
   :name "Viper's Bite"})

(def volatile-schematics
  {:class #{:huntress}
   :description
   ["Trap effect radius increased by "
    [0 3]
    "%."]
   :name "Volatile Schematics"})

;;; Lavamancer

(def bone-glove
  {:class #{:lavamancer}
   :description
   ["Primary Attacks fire piercing bones that deal 25% Hero Damage as magical damage. The bones reflect off surfaces and can hit 3 times."]
   :name "Bone Glove"})

(def burning-finger
  {:class #{:lavamancer}
   :description
   ["Increases Molten Charge damage by "
    [0 5]
    "%."]
   :name "Burning Finger"})

(def crystallized-magma
  {:class #{:lavamancer}
   :description
   ["Increases Lava Maw and Volcano health by "
    [0 5]
    "%."]
   :name "Crystallized Magma"})

(def floor-is-lava
  {:class #{:lavamancer}
   :description
   ["Eruption spawns a lava field dealing "
    [0 500]
    "% Ability Power as Fire magic damage each second for 5s."]
   :name "Floor is Lava"})

(def fossilizer
  {:class #{:lavamancer}
   :description
   ["Increases Lava Maw petrification rate by "
    [0 20]
    "%."]
   :name "Fossilizer"})

(def geddon-blast
  {:class #{:lavamancer}
   :description
   ["Increases Submerge's Emerge damage by "
    [0 5]
    "%"]
   :name "Geddon Blast"})

(def hydrogen-reserve
  {:class #{:lavamancer}
   :description
   ["Increase Molten Core's projectile duration is by "
    [0 1]
    "s."]
   :name "Hydrogen Reserve"})

(def inner-fire
  {:class #{:lavamancer}
   :description
   ["Increases the amount of Molten Power gained by "
    [0 15]
    "%."]
   :name "Inner Fire"})

(def living-bomb
  {:class #{:lavamancer}
   :description
   ["Increases Eruption damage by "
    [0 5]
    "%."]
   :name "Living Bomb"})

(def pyroclast
  {:class #{:lavamancer}
   :description
   ["Increases Volcano damage by "
    [0 5]
    "%."]
   :name "Pyroclast"})

(def seismic-wave
  {:class #{:lavamancer}
   :description
   ["Increases Molten Charge stun duration by "
    [0 1]
    "s."]
   :name "Seismic Wave"})

(def shine-spark
  {:class #{:lavamancer}
   :description
   ["Increases Fissure of Embermount damage by "
    [0 5]
    "%."]
   :name "Shine Spark"})

(def stone-sunshine
  {:class #{:lavamancer}
   :description
   ["Increases Molten Core damage by "
    [0 5]
    "%."]
   :name "Stone Sunshine"})

(def sulfuric-fumes
  {:class #{:lavamancer}
   :description
   ["Increases Oil Geyser damage by "
    [0 5]
    "%."]
   :name "Sulfuric Fumes"})

(def tar-pit
  {:class #{:lavamancer}
   :description
   ["Increases Oil Geyser slow rate by "
    [0 20]
    "%."]
   :name "Tar Pit"})

(def tephra-bomb
  {:class #{:lavamancer}
   :description
   ["Volcano launches a huge rock dealing "
    [0 625]
    "% Defense Power once every "
    ["UNKNOWN" "UNKOWN"]
    " volleys."]
   :name "Tephra Bomb"})

;;; Monk

(def amped-up
  {:class #{:monk}
   :description
   ["Lightning Aura and Lightning Strikes Aura attack rate increased by "
    [0 3]
    "%."]
   :name "Amped Up"})

(def empower
  {:class #{:monk}
   :description
   ["Boost Aura defence power boost is increased by "
    [0 25]
    "%."]
   :name "Empower"})

(def harmony
  {:class #{:monk}
   :description
   ["Heroic Wave healing and Shielding Wave shield are increased by "
    [0 15]
    "%."]
   :name "Harmony"})

(def haunting
  {:class #{:monk}
   :description
   ["Melee attacks add Ghost Stacks. Ghost Stacks increase Hero Damage by 0.5%. At 10 stacks, fire Ghosts dealing "
    [0 50]
    "% physical Hero Damage for 3 seconds. Ghosts spawn once per 5 seconds."]
   :name "Haunting"})

(def idle-flow
  {:class #{:monk}
   :description
   ["Serenity Aura slows enemies inside it by "
    [0 60]
    "%."]
   :name "Idle Flow"})

(def karma-vortex
  {:class #{:monk}
   :description
   ["Serenity Aura's radius increased by "
    [0 60]
    "%."]
   :name "Karme Vortex"})

(def kobold-slayer
  {:class #{:monk}
   :description
   ["Defeating a Kobold grants a charge. Secondary Attacks consumes charges, causing a Kobold Flier to attack the target for "
    [0 150]
    "% of your Hero Damage Stat as physical damage."]
   :name "Kobold Slayer"})

(def mighty-smash
  {:class #{:monk}
   :description
   ["Pole Smash spawns a fist dealing "
    [0 240]
    "% Ability Power as damage. After a short delay, another fist emerges dealing "
    [0 450]
    "% Ability Power as damage."]
   :name "Mighty Smash"})

(def north-pole
  {:class #{:monk}
   :description
   ["Secondary Attacks fire a copy of the North Pole that pierces 2 enemies and slows them by 40% for 6s. As it travels, it drops snowy projectiles that deal the same amount of damage."]
   :name "North Pole"})

(def power-pole
  {:class #{:monk}
   :description
   ["Pole Smash damage increased by "
    [0 3]
    "%."]
   :name "Power Pole"})

(def power-up
  {:class #{:monk}
   :description
   ["Heroic Wave Hero Damage boost is increased by "
    [0 19]
    "%."]
   :name "Power Up"})

(def purge-evil
  {:class #{:monk}
   :description
   ["Defeated enemies inside Serenity Aura explode dealing "
    [0 400]
    "% of defense power as damage. Enemies killed by Purge Evil also explode."]
   :name "Purge Evil"})

(def radiance
  {:class #{:monk}
   :description
   ["Aura radius increased by "
    [0 3]
    "%."]
   :name "Radiance"})

(def smiting-fists
  {:class #{:monk}
   :description
   [[0 50]
    "% chance Lightning Strikes Aura deals "
    [0 400]
    "% Defense Power as damage, then "
    [0 250]
    "% Defense Power as damage to nearby targets."]
   :name "Smiting Fists"})

(def spiral-energy
  {:class #{:monk}
   :description
   ["Chi Blast damage increased by "
    [0 19]
    "%."]
   :name "Spiral Energy"})

(def storm-rider
  {:class #{:monk}
   :description
   ["Glide on a Storm Cloud for "
    [0 30]
    "s drenching enemies under you. Every 2s the Storm Cloud strikes targets for "
    [0 2000]
    "% of Ability Power as Magical Storm Damage. While gliding, your primary attack is ranged lightning bolts that deal "
    [0 500]
    "% of Ability Power as Magical Storm Damage."]
   :name "Storm Rider"})

(def thunderball
  {:class #{:monk}
   :description
   [[0 60]
    "% chance on hit secondary attacks will spawn a lightning area, dealing "
    [0 150]
    "% Hero Damage as Storm Damage, lasting for "
    [0 12]
    "s."]
   :name "Thunderball"})

(def walking-tempest
  {:class #{:monk}
   :description
   ["40% increased movement speed. Basic attacks deal "
    [0 40]
    "% Hero Damage as Magical Storm Damage."]
   :name "Walking Tempest"})

(def zen-monument
  {:class #{:monk}
   :description
   ["Serenity Aura heal rate increased by "
    [0 19]
    "%."]
   :name "Zen Monument"})

;; Mystic

(def chaos-incentive
  {:class #{:mystic}
   :description
   ["Increases Call to Madness duration by 1s."]
   :name "Chaos Incentive"})

(def corrupted-blood
  {:class #{:mystic}
   :description
   ["Increases Lash Out damage by "
    [1 4]
    "%."]
   :name "Corrupted Blood"})

(def fake-idols-ruin
  {:class #{:mystic}
   :description
   ["When a Dark Torment serpent expires, it explodes dealing "
    [100 200]
    "% Hero Health magical damage to nearby enemies."]
   :name "Fake Idol's Ruin"})

(def heathens-prison
  {:class #{:mystic}
   :description
   ["Increases Viper's Fang bubble duration by 1s."]
   :name "Heathen's Prison"})

(def heretics-prayers
  {:class #{:mystic}
   :description
   ["Increases Appeasment gained by "
    [1 8]
    "%."]
   :name "Heretic's Prayers"})

(def nightmare-fuel
  {:class #{:mystic}
   :description
   ["Enemies effected by Lash Out poison take "
    [0 55]
    "% Hero Damage as magical damage when hit by Dark Torment."]
   :name "Nightmare Fuel"})

(def rage-intensify
  {:class #{:mystic}
   :description
   ["Incrases Sand Viper damage by "
    [1 4]
    "%."]
   :name "Rage Intensify"})

(def sand-hazard
  {:class #{:mystic}
   :description
   ["Incrases Snaking Sands damage by "
    [1 4]
    "%."]
   :name "Sand Hazard"})

(def scaled-gods-wrath
  {:class #{:mystic}
   :description
   ["Increases Obelisk Smite damage by "
    [1 4]
    "%."]
   :name "Scaled God's Wrath"})

(def seeping-insanity
  {:class #{:mystic}
   :description
   ["Increases Call to Madness range by "
    [1 10]
    "%."]
   :name "Seeping Insanity"})

(def segmented-extension
  {:class #{:mystic}
   :description
   ["Increases Snake Tower range by "
    [1 8]
    "%."]
   :name "Segmented Extension"})

(def serpents-twin-gaze
  {:class #{:mystic}
   :description
   ["Enemies affected by both Serpent's Coil and Sand Viper recieve "
    [100 200]
    "% Defense Power as magical damage each second."]
   :name "Serpent's Twin Gaze"})

(def shadowflame-dagger
  {:class #{:mystic}
   :description
   ["Secondary Attacks hurl a Shadowflame Dagger dealing 73% Hero Damage as magical damage and will bounce hit up to 3 targets."]
   :name "Shadowflame Dagger"})

(def subdermal-injection
  {:class #{:mystic}
   :description
   ["Increases Lash Out poison duration by 1s."]
   :name "Subderman Injection"})

(def virulent-whispers
  {:class #{:mystic}
   :description
   ["Increases Dark Torment damage by "
    [1 4]
    "%"]
   :name "Virulent Whispers"})

(def woven-scales
  {:class #{:mystic}
   :description
   ["Increases Viper's Fangs health by "
    [1 5]
    "%."]
   :name "Woven Scales"})

;;; Squire

(def aftershock
  {:class #{:squire}
   :description
   ["Seismic Slam stun duration increased by "
    [0 19]
    "%."]
   :name "Aftershock"})

(def annoyance
  {:class #{:squire}
   :description
   ["Taunts nearby enemies every 2s, drawing their attention for 3s."]
   :name "Annoyance"})

(def automation
  {:class #{:squire}
   :description
   ["25% chance on hit Spike Blockade repaired "
    [0 16]
    "% of damage dealt."]
   :name "Automation"})

(def black-arrow
  {:class #{:squire}
   :description
   ["Ballista has a 30% chance on hit to deal "
    [0 30]
    "% extra physical damage."]
   :name "Black Arrow"})

(def drakin-slayer
  {:class #{:squire}
   :description
   ["Defeating a Drakin grants a charge. Blocking with your Shield consumes charges, unleashing fireballs dealing "
    [0 150]
    "% of your Hero Health Stat as magical fire damage each."]
   :name "Drakin Slayer"})

(def full-hearts
  {:class #{:squire}
   :description
   ["Sword Beam and Empowered Beam damage increased by "
    [0 3]
    "%."]
   :name "Full Hearts"})

(def harbingers-fury
  {:class #{:squire}
   :description
   ["During Provoke, hits deal "
    [0 500]
    "% Ability Power unresisted damage over "
    [0 3]
    "s, and Seismic Slam damage increased by "
    [0 300]
    "% Ability Power"]
   :name "Harbinger's Fury"})

(def hearty-blockade
  {:class #{:squire}
   :description
   [[0 40]
    "% of Defence Health is added to the Spike Blockade's Defence Health"]
   :name "Hearty Blockade"})

(def howling
  {:class #{:squire}
   :description
   ["Attacks add feast stacks. Each feast stack increases Hero Damage by 0.5%. At 10 stacks, Howls, changing weapon speed and multiplying Hero Damage by "
    [1 1.15]
    " until a stack falls off."]
   :name "Howling"})

(def inspirational
  {:class #{:squire}
   :description
   ["Nearby defenses gain "
    [0 600]
    " Defense Power."]
   :name "Inspirational"})

(def meowmere
  {:class #{:squire :dryad}
   :description
   ["Once per second, Primary Attacks fire the piercing Meowmere projectile dealing 80% Ability Power as magical damage and reflecting off surfaces for 3 hits. Projectiles explode on any impact dealing 40% Ability Power as magical damage."]
   :name "Meowmere"})

(def iron-core
  {:class #{:squire}
   :description
   ["Cannonball and Heavy Cannonball Tower damage increased by "
    [0 3]
    "%."]
   :name "Iron Core"})

(def null-void
  {:class #{:squire}
   :description
   ["Training Dummy deals "
    [0 80]
    "% Defense Health as unresisted damage. "
    [0 17]
    "% chance per hit to teleport enemy back to lane start."]
   :name "Null Void"})

(def shellshock
  {:class #{:squire}
   :description
   ["Cannonball Tower has a "
    [0 25]
    "% chance on hit to stun for 2s."]
   :name "Shellshock"})

(def slamming-block
  {:class #{:squire}
   :description
   ["Shield blocks reduce the cooldown of Seismic Slam by "
    [0 1]
    "s."]
   :name "Slamming Block"})

(def sonic-boom
  {:class #{:squire}
   :description
   ["Attacking in the air fires a sonic projectile that deals "
    [0 200]
    "% of your Hero Damage as damage."]
   :name "Sonic Boom"})

(def speedy-harpoon
  {:class #{:squire}
   :description
   ["Increases the Ballista's Defense Speed by "
    [0 100]
    "."]
   :name "Speedy Harpoon"})

(def splody-harpoon
  {:class #{:squire}
   :description
   ["Ballista projectiles detonate on hit and deal "
    [0 250]
    "% of Defense Power as damage around the enemy hit."]
   :name "Splody Harpoon"})

(def terra-blade
  {:class #{:squire :dryad}
   :description
   ["Once per second, Terra Blade fires an epic wisp that pierces up to 3 targest dealing 50% Hero Damage as magical damage."]
   :name "Terra Blade"})

(def trollface
  {:class #{:squire}
   :description
   ["Provoke duration increased by "
    [0 7]
    "s."]
   :name "Trollface"})

(def unholy-fire
  {:class #{:squire}
   :description
   ["Blocking unleashes a flame dealing "
    [0 600]
    "% Ability Power magical fire damage twice per second."]
   :name "Unholy Fire"})

;; Storms Set

(def taser-suit
  {:class #{:abyss-lord :apprentice :ev2 :gun-witch :huntress :lavamancer :monk :squire}
   :description
   [[0 15]
    "% chance when damaged to electrocute enemies in a "
    [0 1000]
    "radius for "
    [0 4]
    " s."]
   :name "Taser Suit"})

(def gloves-of-storm
  {:class #{:abyss-lord :apprentice :ev2 :gun-witch :huntress :lavamancer :monk :squire}
   :description
   ["Once per second, basic attacks spawn a lightning ball that deals "
    [0 25]
    "% Hero Damage as storm damage and bounces up to "
    [0 4]
    " times."]
   :name "Gloves of Storm"})

(def helm-of-storms
  {:class #{:abyss-lord :apprentice :ev2 :gun-witch :huntress :lavamancer :monk :squire}
   :description
   ["Increases mana capacity by +"
    [50 100]
    " and "
    [10 20]
    "% chance on dealing damage to restore mana at an alarming rate for "
    [1 3]
    " seconds"]
   :name "Helm of Storms"})

(def storm-boots
  {:class #{:abyss-lord :apprentice :ev2 :gun-witch :huntress :lavamancer :monk :squire}
   :description
   [[0 100]
    "% chance when landing after a jump to deal "
    [0 250]
    "% Abilty Power as storm damage to nearby enemies. Movement speed increased by "
    [0 800]
    "."]
   :name "Storm Boots"})
