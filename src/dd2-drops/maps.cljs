(ns dd2-drops.maps
  (:require [dd2-drops.items :as items]))

;; Defense Maps

(def the-gates-of-dragonfall
  {:image "the-gates-of-dragonfall.png"
   :name "The Gates of Dragonfall"
   :type :defense})

(def dragonfall-bazaar
  {:image "dragonfall-bazaar.png"
   :name "Dragonfall Bazaar"
   :type :defense})

(def siphon-site-d
  {:image "siphon-site-d.png"
   :name "Siphon Site D"
   :drops
   [items/huntress-harbingers-fist-tower]
   :type :defense})

(def dragonfall-sewers
  {:image "dragonfall-sewers.png"
   :name "Dragonfall Sewers"
   :drops
   [items/monk-harbingers-punch-tower]
   :type :defense})

(def forgotten-ruins
  {:image "forgotten-ruins.png"
   :name "Forgotten Ruins"
   :drops
   [items/squire-harbinger-sword-hero]
   :type :defense})

(def nimbus-reach
  {:image "nimbus-reach.png"
   :name "Nimbus Reach"
   :drops
   [items/storm-boots]
   :type :defense})

(def little-horn-valley
  {:image "little-horn-valley.png"
   :name "Little-Horn Valley"
   :type :defense})

(def greystone-plaza
  {:image "greystone-plaza.png"
   :name "Greystone Plaza"
   :drops
   [items/storm-chest]
   :type :defense})

(def the-ramparts
  {:image "the-ramparts.png"
   :name "The Ramparts"
   :drops
   [items/monk-harbingers-punch-hero]
   :type :defense})

(def the-throne-room
  {:image "the-throne-room.png"
   :name "The Throne Room"
   :drops
   [items/blaster-caster
    items/bow-blaster
    items/crystalline-saber
    items/crystalline-twin-blade]
   :type :defense})

(def forest-crossroads
  {:image "forest-crossroads.png"
   :name "Forest Crossroads"
   :type :defense})

(def liferoot-forest
  {:image "liferoot-forest.png"
   :name "LifeRoot Forest"
   :drops
   [items/apprentice-harbingers-watch-hero]
   :type :defense})

(def the-wyvern-den
  {:image "the-wyvern-den.png"
   :name "The Wyvern Den"
   :drops
   [items/huntress-aerial-bane]
   :type :defense})

(def ramparts-siege
  {:image "ramparts-siege.png"
   :name "Ramparts Siege"
   :drops
   [items/huntress-harbingers-fist-hero]
   :type :defense})

(def assault-on-throne-room
  {:image "assault-on-throne-room.png"
   :name "Assault on Throne Room"
   :drops
   [items/storm-gloves]
   :type :defense})

(def harbingers-warship
  {:image "harbingers-warship.png"
   :name "Harbinger's Warship"
   :drops
   [items/squire-harbinger-shield]
   :type :defense})

(def unholy-catacombs
  {:image "unholy-catacombs.png"
   :name "Unholy Catacombs"
   :type :defense})

(def the-dead-road
  {:image "the-dead-road.png"
   :name "The Dead Road"
   :drops
   [items/squire-harbinger-sword-defense]
   :type :defense})

(def temple-of-the-necrotic
  {:image "temple-of-the-necrotic.png"
   :name "Temple of the Necrotic"
   :drops
   [items/apprentice-harbingers-watch-tower]
   :type :defense})

(def buried-bastille
  {:image "buried-bastille.png"
   :name "Buried Bastille"
   :drops
   [items/storm-helm]
   :type :defense})

(def molten-citadel
  {:image "molten-citadel.png"
   :name "Molten Citadel"
   :type :defense})

;; Incursion Maps

(def gribloks-horde
  {:image "the-gates-of-dragonfall.png"
   :name "Griblok's Horde"
   :drops
   [items/squire-impaling-cutter-sword]
   :type :incursion})

(def power-surge
  {:image "dragonfall-bazaar.png"
   :name "Power Surge"
   :drops
   [items/ipwr-750-boots
    items/monk-glaive-of-the-storms-polearm]
   :type :incursion})

(def chrome-enemies
  {:image "forgotten-ruins.png"
   :name "Chrome Enemies"
   :drops
   [items/huntress-toxic-shock-bow]
   :type :incursion})

(def kobold-bling-king
  {:image "little-horn-valley.png"
   :name "Kobold Bling King"
   :drops
   [items/huntress-bling-o-midas-bow
    items/ipwr-750-gloves]
   :type :incursion})

(def malthius
  {:image "the-ramparts.png"
   :name "Malthius"
   :drops
   [items/monk-infernal-combustor-polearm]
   :type :incursion})

(def wyvern-enthusiasts
  {:image "forest-crossroads.png"
   :name "Wyvern Enthusiasts"
   :drops
   [items/huntress-phantom-phoenix-bow]
   :type :incursion})

(def forest-poachers
  {:image "liferoot-forest.png"
   :name "Forest Poachers"
   :drops
   [items/apprentice-chilling-touch-staff]
   :type :incursion})

(def spectral-knight
  {:image "unholy-catacombs.png"
   :name "Spectral Knight"
   :drops
   [items/ipwr-750-chest
    items/squire-sword-of-unholy-fire]
   :type :incursion})

(def bastille-master
  {:image "buried-bastille.png"
   :name "Bastille Master"
   :drops
   [items/apprentice-ghastly-halberd-staff
    items/ipwr-750-helm]
   :type :incursion})

(def the-demons-lair
  {:image "molten-citadel.png"
   :name "The Demon's Lair"
   :drops
   [items/abyss-lord-molten-tome
    items/ipwr-750-medallion]
   :type :incursion})

;;; Common Hero Items

(def abyss-lord-common
  {:image "abyss-lord.png"
   :name "Abyss Lord Common Items"
   :drops
   [items/abyss-lord-common-items]
   :type :common})

(def apprentice-common
  {:image "apprentice.png"
   :name "Apprentice Common Items"
   :drops
   [items/apprentice-common-items]
   :type :common})

(def ev2-common-items
  {:image "ev2.png"
   :name "EV2 Common Items"
   :drops
   [items/ev2-common-items]
   :type :common})

(def gun-witch-common-items
  {:image "gun-witch.png"
   :name "Gun Witch Common Items"
   :drops
   [items/gun-witch-common-items]
   :type :common})

(def huntress-common-items
  {:image "huntress.png"
   :name "Huntress Common Items"
   :drops
   [items/huntress-common-items]
   :type :common})

(def lavamancer-common-items
  {:image "lavamancer.png"
   :name "Lavamancer Common Items"
   :drops
   [items/lavamancer-common-items]
   :type :common})

(def monk-common-items
  {:image "monk.png"
   :name "Monk Common Items"
   :drops
   [items/monk-common-items]
   :type :common})

(def mystic-common-items
  {:image "mystic.png"
   :name "Mystic Common Items"
   :drops
   [items/mystic-common-items]
   :type :common})

(def squire-common
  {:image "squire.png"
   :name "Squire Common Items"
   :drops
   [items/squire-common-items]
   :type :common})

;; Shops

(def wayfarers-shop
  {:image "wayfarers-shop.png"
   :name "Wayfarer's Shop"
   :drops
   [items/terraria-bone-glove
    items/terraria-celebration
    items/terraria-demon-scythe
    items/terraria-megashark
    items/terraria-lunar-portal
    items/terraria-meowmere
    items/terraria-north-pole
    items/terraria-shadowflame-knife
    items/terraria-terra-blade
    items/terraria-tsunami-chlorophyte-arrows
    items/terraria-tsunami-jester-arrows]
   :type :common})

(def maps
  [;; Common Hero Stats
   abyss-lord-common
   apprentice-common
   ev2-common-items
   gun-witch-common-items
   huntress-common-items
   lavamancer-common-items
   monk-common-items
   mystic-common-items
   squire-common
   ;; Defense Maps
   the-gates-of-dragonfall
   dragonfall-bazaar
   siphon-site-d
   dragonfall-sewers
   forgotten-ruins
   nimbus-reach
   little-horn-valley
   greystone-plaza
   the-ramparts
   the-throne-room
   forest-crossroads
   liferoot-forest
   the-wyvern-den
   ramparts-siege
   assault-on-throne-room
   harbingers-warship
   unholy-catacombs
   the-dead-road
   temple-of-the-necrotic
   buried-bastille
   molten-citadel
   ;; Incursion Maps
   wyvern-enthusiasts
   malthius
   gribloks-horde
   chrome-enemies
   forest-poachers
   kobold-bling-king
   spectral-knight
   bastille-master
   power-surge
   the-demons-lair
   ;; Shops
   wayfarers-shop])
