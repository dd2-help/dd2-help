(ns dd2-drops.searching
  (:require [clojure.set :refer [intersection union]]
            [clojure.string :refer [includes? lower-case replace]]))

(defn description->str
  [description]
  (reduce
    (fn [acc next]
      (if (string? next)
        (str acc next)
        acc))
    ""
    description))

(defn str-contains?
  [haystack needle]
  (when (and haystack needle)
    (let [haystack' (replace (lower-case haystack) #"[^a-z0-9 ]" "")
          needle' (replace (lower-case needle) #"[^a-z0-9 ]" "")]
      (includes? haystack' needle'))))

(defn relevant-map?
  [dd2-map filters]
  (let [{:keys [class-type map-type search]} filters
        {:keys [drops name type]} dd2-map
        passives (reduce into #{} (map :passives drops))
        drop-names (map :name drops)
        classes (reduce into #{} (map :class passives))
        descriptions (map (comp description->str :description) passives)]
    (and
      ;; The map type must match the filters.
      (map-type type)
      ;; There must be a passive for a class in the filters.
      (pos? (count (intersection class-type classes)))
      ;; The search string (if any) must match something.
      (or
        (empty? search)
        (str-contains? name search)
        (some #(str-contains? % search) drop-names)
        (some #(str-contains? % search) descriptions)))))
