(ns dd2-drops.items
  (:require [dd2-drops.passives :as passives]))

(def abyss-lord-common-items
  {:name
   "Abyss Lord Common Passives"
   :passives
   [passives/abyss-knight-damage
    passives/abyss-knights-mana-cost
    passives/abyss-stone-mana-cost
    passives/archer-damage
    passives/breath-damage
    passives/colossal-health
    passives/direct-command-mana-cost
    passives/electric-fingers
    passives/explosive-arrows
    passives/ramster-breath-range
    passives/shattering-stones
    passives/skeletal-orc-health]})

(def abyss-lord-molten-tome
  {:name "Abyss Lord Molten Tome"
   :passives
   [passives/devour
    passives/emburst-assault]})

(def apprentice-armageddon-staff
  {:name "Apprentice Armageddon Staff"
   :passives
   [passives/weaver]})

(def apprentice-chilling-touch-staff
  {:name "Chilling Touch"
   :passives
   [passives/frosty-power]})

(def apprentice-common-items
  {:name "Apprentice Common Passives"
   :passives
   [passives/ancient-magics
    passives/arcanist
    passives/fault-line
    passives/frosty-power
    passives/mighty-wind
    passives/pyromania
    passives/snowstorm
    passives/starlight-burst]})

(def apprentice-ghastly-halberd-staff
  {:name "Ghastly Halberd"
   :passives
   [passives/ghostly-aura
    passives/specter-scepters]})

(def apprentice-harbingers-watch-hero
  {:name "Harbinger's Watch"
   :passives
   [passives/mana-portal]})

(def apprentice-harbingers-watch-tower
  {:name "Harbinger's Watch"
   :passives
   [passives/arcane-resilience]})

(def blaster-caster
  {:name "Blaster-Caster"
   :passives
   [passives/ravenhost]})

(def bow-blaster
  {:name "Bow-Blaster"
   :passives
   [passives/call-of-the-phoenix
    passives/from-the-ashes]})

(def crystalline-saber
  {:name "Crystalline Saber"
   :passives
   [passives/splody-harpoon]})

(def crystalline-twin-blade
  {:name "Crystalline Twin-Blade"
   :passives
   [passives/haunting]})

(def ev2-common-items
  {:name "EV2 Common Passives"
   :passives
   [passives/atomic-madness
    passives/big-boy
    passives/deathly-heat
    passives/dummy-damage
    passives/frosty-proton-node
    passives/grav-bot-capacity-increase
    passives/grav-bot-compensator
    passives/grav-bot-detonation-radius
    passives/mdl-discharge
    passives/proton-cannon-heat-generation
    passives/proton-charge-width
    passives/reflect-beam-nodes
    passives/secondary-heat
    passives/torpedo-range
    passives/weapon-manufacturer-charge-rate
    passives/weapon-manufacturer-damage]})

(def gun-witch-common-items
  {:name "Gun Witch Common Passives"
   :passives
   [passives/a360-no-scope
    passives/bat-shot
    passives/broom-bash
    passives/clean-sweep
    passives/deadeye
    passives/focus-fire
    passives/frozen-core
    passives/magazine-reserve
    passives/skeet-shooter
    passives/sorcerors-apprentice
    passives/witch-mark
    passives/witch-recover
    passives/zerod-in]})

(def huntress-aerial-bane
  {:name "Aerial Bane"
   :passives
   [passives/mirv]})

(def huntress-armored-cleanser-bow
  {:name "Huntress Armored Cleanser Bow"})

(def huntress-bling-o-midas-bow
  {:name "Bling-O-Midas"
   :passives
   [passives/gold-shower
    passives/midas-touch]})

(def huntress-common-items
  {:name "Huntress Common Passives"
   :passives
   [passives/envenom
    passives/flameageddon
    passives/flashbang
    passives/shrapnel
    passives/tripwire
    passives/vicious-brew
    passives/victory-smell
    passives/volatile-schematics]})

(def huntress-harbingers-fist-tower
  {:name "Harbinger's Fist"
   :passives
   [passives/shard-spike]})

(def huntress-harbingers-fist-hero
  {:name "Harbinger's Fist"
   :passives
   [passives/shard-shot]})

(def huntress-harbinger-bow-hero
  {:name "Huntress Harbinger Bow Hero"})

(def huntress-phantom-phoenix-bow
  {:name "Phantom Phoenix"
   :passives
   [passives/call-of-the-phoenix
    passives/from-the-ashes]})

(def huntress-toxic-shock-bow
  {:name "Toxic Shock"
   :passives
   [passives/vipers-bite]})

(def ipwr-750-boots
  {:name "iPwr 750 Boots"})

(def ipwr-750-chest
  {:name "iPwr 750 Chest"})

(def ipwr-750-gloves
  {:name "iPwr 750 Gloves"})

(def ipwr-750-helm
  {:name "iPwr 750 Helm"})

(def ipwr-750-medallion
  {:name "iPwr 750 Medallion"})

(def lavamancer-common-items
  {:name
   "Lavamancer Common Passives"
   :passives
   [passives/burning-finger
    passives/crystallized-magma
    passives/floor-is-lava
    passives/fossilizer
    passives/geddon-blast
    passives/hydrogen-reserve
    passives/inner-fire
    passives/living-bomb
    passives/pyroclast
    passives/seismic-wave
    passives/shine-spark
    passives/stone-sunshine
    passives/sulfuric-fumes
    passives/tar-pit
    passives/tephra-bomb]})

(def monk-common-items
  {:name "Monk Common Passives"
   :passives
   [passives/amped-up
    passives/empower
    passives/harmony
    passives/idle-flow
    passives/karma-vortex
    passives/power-pole
    passives/power-up
    passives/radiance
    passives/spiral-energy
    passives/zen-monument]})

(def monk-glaive-of-the-storms-polearm
  {:name "Glaive of the Storms"
   :passives
   [passives/storm-rider
    passives/walking-tempest]})

(def monk-harbingers-punch-tower
  {:name "Harbinger's Punch"
   :passives
   [passives/smiting-fists]})

(def monk-harbingers-punch-hero
  {:name "Harbinger's Punch"
   :passives
   [passives/mighty-smash]})

(def monk-infernal-combustor-polearm
  {:name "Infernal Combuster"
   :passives
   [passives/purge-evil]})

(def mystic-common-items
  {:name "Mystic Common Items"
   :passives
   [passives/chaos-incentive
    passives/corrupted-blood
    passives/fake-idols-ruin
    passives/heathens-prison
    passives/heretics-prayers
    passives/nightmare-fuel
    passives/rage-intensify
    passives/sand-hazard
    passives/scaled-gods-wrath
    passives/seeping-insanity
    passives/segmented-extension
    passives/serpents-twin-gaze
    passives/subdermal-injection
    passives/virulent-whispers
    passives/woven-scales]})

(def squire-common-items
  {:name "Squire Common Passives"
   :passives
   [passives/aftershock
     passives/automation
     passives/black-arrow
     passives/full-hearts
     passives/hearty-blockade
     passives/iron-core
     passives/shellshock
     passives/trollface]})

(def squire-harbinger-shield
  {:name "Harbinger's Portal"
   :passives
   [passives/slamming-block]})

(def squire-harbinger-sword-defense
  {:name "Harbinger's Voidreaver"
   :passives
   [passives/null-void]})

(def squire-harbinger-sword-hero
  {:name "Harbinger's Voidreaver"
   :passives
   [passives/harbingers-fury]})

(def squire-impaling-cutter-sword
  {:name "Impaling Cutter"
   :passives
   [passives/splody-harpoon]})

(def squire-scarlet-enforcer-sword
  {:name "Squire Scarlet Enforcer Sword"
   :passives
   [passives/inspirational]})

(def squire-sword-of-unholy-fire
  {:name "Sword of Unholy Fire"
   :passives
   [passives/annoyance
    passives/unholy-fire]})

(def storm-boots
  {:name "Boots of Storms"
   :passives
   [passives/storm-boots]})

(def storm-chest
  {:name "Chest of Storms"
   :passives
   [passives/taser-suit]})

(def storm-gloves
  {:name "Gloves of Storms"
   :passives
   [passives/gloves-of-storm]})

(def storm-helm
  {:name "Helm of Storms"
   :passives
   [passives/helm-of-storms]})

(def terraria-bone-glove
  {:name "Bone Glove"
   :passives
   [passives/bone-glove]})

(def terraria-celebration
  {:name "Celebration!!!"
   :passives
   [passives/celebration]})

(def terraria-demon-scythe
  {:name "Demon Scythe"
   :passives
   [passives/demon-scythe]})

(def terraria-megashark
  {:name "Megashark"
   :passives
   [passives/meteor-bullets]})

(def terraria-lunar-portal
  {:name "Lunar Portal Staff"
   :passives
   [passives/lunar-portal]})

(def terraria-meowmere
  {:name "Meowmere"
   :passives
   [passives/meowmere]})

(def terraria-north-pole
  {:name "North Pole"
   :passives
   [passives/north-pole]})

(def terraria-shadowflame-knife
  {:name "Shadowflame Knife"
   :passives
   [passives/shadowflame-dagger]})

(def terraria-terra-blade
  {:name "Terra Blade"
   :passives
   [passives/terra-blade]})

(def terraria-tsunami-chlorophyte-arrows
  {:name "Tsunami"
   :passives
   [passives/chlorophyte-arrows]})

(def terraria-tsunami-jester-arrows
  {:name "Tsunami"
   :passives
   [passives/jester-arrows]})
