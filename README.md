# DD2 Drops Static Website

[DD2.help](http://dd2.help)

> A simple database of dropped items in DD2.

## Purpose

This project compiles to a static website helping you figure out which map to farm if you're looking for a specific item in Dungeon Defenders 2.

You can have this website open on your phone, tablet, secondary monitor or the likes while playing DD2 to easily figure out where to go for which dropped items.

## Requirements to build

You'll need [Leiningen](http://leiningen.org/#install) installed, and that's really it.

## Develop locally

The easiest way to develop the project locally is to run "`lein do clean, figwheel`" in a terminal to compile the app, and then open `http://localhost:3449/index.html`.

Any changes to ClojureScript source files (in `src`) will be reflected in the running page immediately (while "`lein figwheel`" is running).

Run "`lein do clean, with-profile prod compile`" to compile an optimized version, and then open `resources/public/index.html`.

Original reagent example code found at https://github.com/reagent-project/reagent

## Deployment

The project automatically compiles and deploys [the static website](https://emiln.gitlab.io/dd2-pages) when Merge Requests are accepting into the `master` branch.
